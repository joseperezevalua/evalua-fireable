<?php

namespace Evalua\Fireable;

use Evalua\Fireable\Exceptions\FireableException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use RuntimeException;

class Fireable
{
    /**
     * @var array
     */
    protected $fireableAttributes = [];

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Match updated attributes with fireable ones and trigger events.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function processAttributes(Model $model)
    {
        $this->model = $model;
        $this->fireableAttributes = $this->model->getFireableAttributes();
        $updatedAttributes = $this->getUpdatedFireableAttributes();

        $this->fireEvents($updatedAttributes);
    }

    /**
     * Get a list of the attributes that were updated and have specified events.
     *
     * @return array
     */
    private function getUpdatedFireableAttributes()
    {
        $updatedAttributes = $this->model->getDirty();
        $updatedFireableAttributes = Arr::only($updatedAttributes, array_keys($this->fireableAttributes));

        return $updatedFireableAttributes;
    }

    /**
     * Dispatch events for matched attributes.
     *
     * @param array $updatedAttributes 
     * @return void
     */
    private function fireEvents(array $updatedAttributes)
    {
        foreach ($updatedAttributes as $attribute => $value) {
            if ($eventName = $this->getEventName($attribute, $value)) {
                try {
                    App::make($eventName)->fire();
                } catch (RuntimeException $e) {
                    throw new FireableException('An Error occurred when firing event '. $eventName, $e->getCode(), $e);
                }
            }
        }
    }

    /**
     * Get event name for specified attribute and assigned value pair.
     *
     * @param string $attribute
     * @param mixed $value
     * @return string|null
     */
    private function getEventName(string $attribute, $value)
    {
        return $this->getEventNameForAttribute($attribute)
            ?? $this->getEventNameForExactValue($attribute, $value);
    }

    /**
     * Get event name if values are not specified.
     *
     * @param string $attribute
     * @return string|null
     */
    private function getEventNameForAttribute(string $attribute)
    {
        return is_string($this->fireableAttributes[$attribute])
            && class_exists($this->fireableAttributes[$attribute])
            ? $this->fireableAttributes[$attribute]
            : null;
    }

    /**
     * Get event name if there are specified values.
     *
     * @param string $attribute
     * @param mixed $value
     * @return string|null
     */
    private function getEventNameForExactValue(string $attribute, $value)
    {
        return is_array($this->fireableAttributes[$attribute])
            && isset($this->fireableAttributes[$attribute][$value])
            && class_exists($this->fireableAttributes[$attribute][$value])
            ? $this->fireableAttributes[$attribute][$value]
            : null;
    }
}
