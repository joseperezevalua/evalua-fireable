<?php

namespace Evalua\Fireable;

interface FireableEvent
{
	public function fire($model);
}

