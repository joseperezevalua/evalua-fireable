# Fireable

An elegant way to trigger events based on attributes changes.

## Installation

Install package through Composer

``` bash
$ composer require envant/fireable
```

## Usage

1. Add the `FireableAttributes` trait to a model
2. Define the attributes with specified events via the `protected $fireableAttributes` property on the model

### Example

Let's say we need to trigger specified events when specific model attributes are updated.

For example, you need to notify user when he gets an "approved" status. Instead of observing model's "dirty" attributes and firing events manually we could do it more elegantly by assigning specified events to attributes or even certain values of attributes.

```php
class Claim extends Model
{
    use FireableAttributes;

    protected $fireableAttributes = [
        'claim_status' => [
            'open' => ClaimOpened::class,
            'finalized' => ClaimFinalized::class,
        ],
    ];
}
```

Also you may not need to track certain values, so you can assign an event directly to an attribute itself. So, in the example below, each time the user's email is changed, the appropriate event will be fired.

```php
class Claim extends Model
{
    use FireableAttributes;

    protected $fireableAttributes = [
        'user_id' => AppraiserUpdated::class,
    ];
}
```
